<?php

/**
 * Metromix service integration
 */
class Metromix
{
  /**
   * Api Key
   */
  public static $apiKey;
  public static $serviceUrl = "http://www.metromix.com/ws/v1/";
  
  public static function searchEvents($keywords, $siteCode, $location, $channel, $page, $pageSize = 25)
  {
    $params = array();
    
    if ($page < 1) $page = 0;
    $page++; // index from 1
    
    $params['keywords'] = $keywords;
    $params['site_code'] = $siteCode;
    $params['page_size'] = $pageSize;
    $params['page'] = $page;
    $params['content_type'] = 'event';
    $params['taxonomies'] = $location;
    
    if ($channel) {
      $params['channel'] = $channel;
    }
    
    $xml = self::doRequest("search_services", $params);

    if ($xml)
    {
      $events = array();
      
      foreach ($xml->search_service->results->event_summary as $eventObj)
      {
        $event = array(
          'id' => (string)$eventObj->id,
          'service_url' => (string)$eventObj->content_service_uri,
          'link' => (string)$eventObj->link,
          'title' => (string)$eventObj->title,
          'description' => strip_tags($eventObj->description),
          'price' => (string)$eventObj->price,
          'phone' => (string)$eventObj->phone,
          'website' => (string)$eventObj->url,
          'image' => (string)$eventObj->image->content_item_image_uri,
          'thumbnail' => (string)$eventObj->image->content_item_thumbnail_uri,
          'channel' => (string)$eventObj->channel->name,
          'subtype' => (string)$eventObj->content_subtypes->content_subtype->name,
          'location' => array(),
          'venue' => NULL,
          'schedule' => NULL,
        );
        
        if (count($eventObj->latitude)) {
          $event['location']['latitude'] = (string)$eventObj->latitude;
          $event['location']['longitude'] = (string)$eventObj->longitude;
        }
        
        if (count($eventObj->addressable)) {
          foreach ($eventObj->addressable->value as $value) {
            $event['location'][(string)$value['name']] = (string) $value;
          }
        }
        
        if (count($eventObj->venue_summary)) {
          $event['venue'] = array(
            'title' => (string)$eventObj->venue_summary[0]->title,
          );
        }
        
        // schedule, most difficult part
        $schedules = $eventObj->schedules;
        if (count($schedules)) {
          $event['schedule']['non_pattern'] = array();
          $event['schedule']['pattern'] = array();
          
          if (count($schedules->non_pattern_schedules)) {
            foreach ($schedules->non_pattern_schedules->non_pattern_schedule as $schedule) {
              $event['schedule']['non_pattern'][] = array(
                'date' => (string)$schedule->occurs_on,
                'start_at' => (string)$schedule->starts_at,
                'end_at' => (string)$schedule->ends_at,
              );
            }
          }
          
          if (count($schedules->schedule_patterns)) {
            foreach ($schedules->schedule_patterns->schedule_pattern as $schedule) {
              $event['schedule']['pattern'][] = array(
                'begin_on' => (string)$schedule->begin_on,
              	'start_at' => (string)$schedule->starts_at,
                'end_at' => (string)$schedule->ends_at,
                'pattern' => (string)$schedule->pattern,
                'printable_day' => (string)$schedule->printable_day,
              );
            }
          }
        }
        
        // fix &amp;
        foreach (array('thumbnail') as $field) {
          $event[$field] = str_replace("&amp;", "&", $event[$field]);
        }
        
        $events[$event['id']] = $event;
      }
      
      $result = array(
        'page' => $page,
        'page_size' => $pageSize,
        'total_items' => (string)$xml->search_service->page_information->total_hits,
        'events' => $events,
      );
      
      $result['pages'] = ceil($result['total_items'] / $result['page_size']);
      
      return $result;
    }
    
    return NULL;
  }
  
  public static function listTaxonomies($path) {
    $params = array();
    
    $params['path'] = $path;
    $xml = self::doRequest("taxonomy_services", $params);
    
  
    if ($xml && count($xml->taxonomy_service->taxonomy)) {
      // traveling to list all regions
      $lists = array($xml->taxonomy_service->taxonomy[0]);
      $result = array();
      
      while (count($lists)) {
        $current = array_shift($lists);
        
        if (count($current->children)) {
          foreach ($current->children->taxonomy as $child) {
            $lists[] = $child;
          }
        }
        
        $result[(string)$current->path] = (string)$current->name;
      }
      
      asort($result);
      
      return $result;
    }
  }
  
  /**
   * Return the list of available channels
   */
  public static function getChannels() {
    return array(
      'event' => 'Event',
      'home' => 'Home',
      'restaurants' => 'Restaurants',
      'bars-and-clubs' => 'Bars and Clubs',
      'music' => 'Music',
      'movies' => 'Movies',
      'deals' => 'Deals',
    );
  }
  
  /**
   * Perform api request
   */
  public static function doRequest($service, $parameters=array())
  {
    $url = self::$serviceUrl . $service . "?key=" . self::$apiKey;
    
    foreach ($parameters as $key => $value) {
      $url .= "&$key=" . rawurlencode($value);
    }
    
    $response = file_get_contents($url);
    
    $xml = simplexml_load_string($response);
    
    return $xml;
  }
}
